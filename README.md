This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Frameworks + Libraries and reasons why

- Create React App: better structure, fast start
- React: fast development, fast application, better application structure - I don't know Vue and Angular.
- Material-UI: fast creation of components, also looks very nice, I still had to write some CSS
- Redux: Wanted to practice React + Redux and also I find it efficient for state management - maybe not so much for smaller applications
- ESlint and Prettier: better code quaility

In the project directory, you can run:

## Time spent and todo's

I have created the app within 2 days. Totally it took me ~ 10 hours. With some breaks.
I did not finish everything, **Tests**, **propTypes** and **error handling** are missing, but the application is ready for adding them later on.

## Deployment and local environment

### `npm install`

Will install dependecies.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `app is deployed to surge`

Open [http://expensive-note.surge.sh/current](http://expensive-note.surge.sh/current) to view it in the browser.

###

## Usage

Hopefully self explanatory.
