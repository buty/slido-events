import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import RestoreIcon from "@material-ui/icons/Restore";
import UpdateIcon from "@material-ui/icons/Update";
import AddIcon from "@material-ui/icons/Add";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import { editActions } from "../state/edit";

const useStyles = makeStyles(() => ({
  container: {
    width: "100%",
    position: "fixed",
    bottom: 0,
    left: 0,
  },
}));

const Navbar = () => {
  const [value, setValue] = useState("recents");
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    const pathName = location.pathname.replace("/", "");
    setValue(pathName);
  }, [location]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    // create empty event
    if (newValue === "edit") dispatch(editActions.initNewEvent());
    history.push(`/${newValue}`);
  };

  return (
    <Paper elevation={3} className={classes.container}>
      <BottomNavigation value={value} onChange={handleChange} showLabels>
        <BottomNavigationAction
          className={classes.navItem}
          value="recent"
          label="Recent"
          icon={<RestoreIcon />}
        />
        <BottomNavigationAction
          className={classes.navItem}
          value="current"
          label="Current"
          icon={<UpdateIcon />}
        />
        <BottomNavigationAction
          className={classes.navItem}
          value="edit"
          label="Create"
          icon={<AddIcon />}
        />
      </BottomNavigation>
    </Paper>
  );
};

export default Navbar;
