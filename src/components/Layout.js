import React from "react";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Navbar from "./Navbar";

const useStyles = makeStyles(() => ({
  content: {
    width: "100%",
    paddingBottom: 56,
  },
}));

const Layout = ({ children }) => {
  const classes = useStyles();

  return (
    <Container maxWidth="md">
      <div className={classes.content}>{children}</div>
      <Navbar />
    </Container>
  );
};

export default Layout;
