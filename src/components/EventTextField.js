import React from "react";
import { useDispatch } from "react-redux";
import TextField from "@material-ui/core/TextField";
import { editActions } from "../state/edit";

const EventTextField = (props) => {
  const { value, type, disabled, className, error } = props;
  const dispatch = useDispatch();

  const handleChange = (type) => (event) => {
    dispatch(editActions.onChangeEvent(type, event.target.value));
  };

  // todo: refactor
  if (type === "dateAndTime") {
    return (
      <TextField
        id="datetime-local"
        label="Date and Time"
        type="datetime-local"
        disabled={disabled}
        value={value}
        onChange={handleChange(type)}
        className={className}
        required
        InputLabelProps={{
          shrink: true,
        }}
      />
    );
  }

  return (
    <TextField
      label={type}
      className={className}
      value={value}
      disabled={disabled}
      onChange={handleChange(type)}
      margin="normal"
      required
      error={error}
      helperText={error}
    />
  );
};

export default EventTextField;
