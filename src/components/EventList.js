import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import EventCard from "./EventCard";
import Loader from "./Loader";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(3),
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const EventList = (props) => {
  const { events, loading } = props;
  const classes = useStyles();

  if (!loading && events) {
    return (
      <div className={classes.container}>
        {events.map((event) => (
          <EventCard data={event} key={event.id} />
        ))}
      </div>
    );
  }

  return (
    <div className={classes.container}>
      <Loader />
    </div>
  );
};

export default EventList;
