import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import EventNoteIcon from "@material-ui/icons/EventNote";
import { formatDateAndTime } from "../utils";
import { editActions } from "../state/edit";

const useStyles = makeStyles((theme) => ({
  container: {
    flexGrow: 1,
    width: "100%",
    maxWidth: 600,
    marginBottom: theme.spacing(2),
    position: "relative",
    cursor: "pointer",
    transition: "box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
    "&:hover": {
      boxShadow:
        "0px 5px 6px -3px rgba(0,0,0,0.2), 0px 9px 12px 1px rgba(0,0,0,0.14), 0px 3px 16px 2px rgba(0,0,0,0.12)",
    },
  },
  paper: {
    padding: theme.spacing(2),
    paddingBottom: theme.spacing(4),
    paddingTop: theme.spacing(4),
  },
  content: {
    display: "flex",
    marginRight: theme.spacing(2),
    justifyContent: "space-around",
    [theme.breakpoints.up("sm")]: {
      marginRight: theme.spacing(6),
    },
  },
  text: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    [theme.breakpoints.up("sm")]: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
    },
  },
  eventIcon: {
    backgroundColor: theme.palette.secondary.main,
    marginRight: theme.spacing(2),
  },
  more: {
    position: "absolute",
    top: 0,
    right: 0,
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
}));

const EventCard = (props) => {
  const { data } = props;
  const { id, title, location, dateAndTime } = data;
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const handleClick = (id) => {
    dispatch(editActions.loadEventById(id));
    history.push(`/edit/${id}`);
  };

  return (
    <div className={classes.container} onClick={() => handleClick(id)}>
      <Paper className={classes.paper}>
        <div className={classes.content}>
          <Avatar className={classes.eventIcon}>
            <EventNoteIcon />
          </Avatar>
          <div className={classes.text}>
            <Typography variant="h6" component="h2">
              {title}
            </Typography>
            <Typography color="textSecondary">
              {formatDateAndTime(dateAndTime)}
            </Typography>
            <Typography variant="body2" component="p">
              {location}
            </Typography>
          </div>
        </div>
        <ButtonBase className={classes.more}>
          <Typography color="textSecondary">
            <MoreVertIcon />
          </Typography>
        </ButtonBase>
      </Paper>
    </div>
  );
};

export default EventCard;
