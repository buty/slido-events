const CREATE_EVENT_INIT = "CREATE_EVENT_INIT";
const CREATE_EVENT_EDITING = "CREATE_EVENT_EDITING";
const CREATE_EVENT_DONE = "CREATE_EVENT_DONE";
const CREATE_EVENT_LOADING = "CREATE_EVENT_LOADING";

const VIEW_EVENT_LOADING = "VIEW_EVENT_LOADING";
const VIEW_EVENT_DONE = "VIEW_EVENT_DONE";

export default {
  CREATE_EVENT_INIT,
  CREATE_EVENT_EDITING,
  CREATE_EVENT_DONE,
  CREATE_EVENT_LOADING,
  VIEW_EVENT_LOADING,
  VIEW_EVENT_DONE,
};
