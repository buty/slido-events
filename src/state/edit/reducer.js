import actionTypes from "./actionTypes";
import { validateEvent } from "../../utils";

const DEFAULT_EVENT = {
  id: "",
  title: "",
  location: "",
  dateAndTime: "",
};

const INITIAL_STATE = {
  isCreation: false,
  loading: false,
  event: DEFAULT_EVENT,
  validation: {
    isValid: false,
    errors: {},
  },
};

export default function event(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case actionTypes.CREATE_EVENT_INIT: {
      return {
        ...INITIAL_STATE,
        isCreation: true,
      };
    }
    case actionTypes.CREATE_EVENT_EDITING: {
      const updatedEvent = {
        ...state.event,
        [payload.field]: payload.value,
      };
      const validator = validateEvent(
        updatedEvent,
        payload.field,
        state.validation
      );
      return {
        ...state,
        isCreation: true,
        event: updatedEvent,
        validation: {
          isValid: validator.isValid,
          errors: {
            ...validator.errors,
          },
        },
      };
    }
    case actionTypes.CREATE_EVENT_LOADING: {
      return {
        ...state,
        isCreation: true,
        loading: true,
      };
    }
    case actionTypes.CREATE_EVENT_DONE: {
      return {
        ...INITIAL_STATE,
        isCreation: true,
        loading: false,
      };
    }
    case actionTypes.VIEW_EVENT_LOADING: {
      return {
        ...state,
        loading: true,
        isCreation: false,
      };
    }
    case actionTypes.VIEW_EVENT_DONE: {
      return {
        ...state,
        event: payload,
        loading: false,
        isCreation: false,
      };
    }
    default:
      return state;
  }
}
