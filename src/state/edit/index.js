import editReducer from "./reducer";
import editActiontypes from "./actionTypes";
import editActions from "./actions";
import editSelectors from "./selectors";

export { editActiontypes, editActions, editSelectors };

export default editReducer;
