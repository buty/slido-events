import actionTypes from "./actionTypes";
import { getEventById, createEvent } from "../../data/events";

/*
 * event details
 */
const viewEventLoading = () => {
  return {
    type: actionTypes.VIEW_EVENT_LOADING,
  };
};

const viewEventDone = (event) => {
  return {
    type: actionTypes.VIEW_EVENT_DONE,
    payload: event,
  };
};

const loadEventById = (id) => async (dispatch) => {
  dispatch(viewEventLoading());
  try {
    const event = await getEventById(id);
    dispatch(viewEventDone(event));
  } catch {
    //todo:
    alert("Error: event can't be loaded");
  }
};

/*
 * create event
 */
const initNewEvent = () => {
  return {
    type: actionTypes.CREATE_EVENT_INIT,
  };
};

const onChangeEvent = (field, value) => {
  return {
    type: actionTypes.CREATE_EVENT_EDITING,
    payload: {
      field,
      value,
    },
  };
};

const createEventLoading = () => {
  return {
    type: actionTypes.CREATE_EVENT_LOADING,
  };
};

const createEventDone = () => {
  return {
    type: actionTypes.CREATE_EVENT_DONE,
  };
};

const saveEvent = (event) => async (dispatch) => {
  dispatch(createEventLoading());
  try {
    await createEvent(event);
    dispatch(createEventDone());
    alert("Event created");
  } catch {
    //todo:
    alert("Error: event can't be created");
  }
};

export default {
  loadEventById,
  initNewEvent,
  onChangeEvent,
  saveEvent,
};
