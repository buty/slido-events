import actionTypes from "./actionTypes";

const INITIAL_STATE = {
  loading: false,
  error: null,
  events: [],
};

export default function events(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case actionTypes.LOAD_EVENTS_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case actionTypes.LOAD_EVENTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        events: payload,
      };
    }
    case actionTypes.LOAD_EVENTS_FAILED: {
      return {
        ...state,
        loading: false,
        error: payload,
      };
    }
    default:
      return state;
  }
}
