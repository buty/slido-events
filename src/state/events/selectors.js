const selectEvents = (state) => state.events;

const selectCurrentEvents = (events) => {
  const today = new Date();
  return events.filter((event) => new Date(event.dateAndTime) > today);
};

const selectExpiredEvents = (events) => {
  const today = new Date();
  return events.filter((event) => new Date(event.dateAndTime) < today);
};

export default {
  selectCurrentEvents,
  selectEvents,
  selectExpiredEvents,
};
