import actionTypes from "./actionTypes";
import { getEvents } from "../../data/events";

const loadEventsSuccess = (events) => {
  return {
    type: actionTypes.LOAD_EVENTS_SUCCESS,
    payload: events,
  };
};

const loadEventsFailed = (error) => {
  return {
    type: actionTypes.LOAD_EVENTS_FAILED,
    paylaod: error,
  };
};

const loadEventsRequest = () => {
  return {
    type: actionTypes.LOAD_EVENTS_REQUEST,
  };
};

const loadAllEvents = () => async (dispatch) => {
  dispatch(loadEventsRequest());
  try {
    const events = await getEvents();
    dispatch(loadEventsSuccess(events));
  } catch {
    //todo:
    dispatch(loadEventsFailed("Error: events can't be loaded"));
  }
};

export default {
  loadAllEvents,
};
