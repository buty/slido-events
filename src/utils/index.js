export const formatDateAndTime = (dateAndTime) => dateAndTime.replace("T", " ");

export const validateEvent = (event, field, validationState) => {
  const validation = validationState;

  if (!event[field]) {
    validation.errors[field] = `${field} is required`;
  } else {
    delete validation.errors[field];
  }
  const emptyFields = Object.values(event).filter((value) => value === "");
  const noEmptyFields = emptyFields.length === 0;
  const noErrors = Object.keys(validation.errors).length === 0;

  return {
    ...validation,
    isValid: noErrors && noEmptyFields,
  };
};
