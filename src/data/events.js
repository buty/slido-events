const TIMEOUT = 100;

const events = [
  {
    id: "1",
    title: "Workshop Meeting",
    dateAndTime: "2017-05-24T10:30",
    location: "Bratislava",
  },
  {
    id: "2",
    title: "Birthday",
    dateAndTime: "2018-05-24T10:30",
    location: "London",
  },
  {
    id: "1",
    title: "Beer Time",
    dateAndTime: "2021-05-24T10:30",
    location: "Telgart",
  },
];

export function getEvents() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(events);
    }, TIMEOUT);
  });
}

export function getEventById(id) {
  return new Promise((resolve) => {
    setTimeout(() => {
      const event = events.find((item) => item.id === id);
      resolve(event);
    }, TIMEOUT);
  });
}

export function createEvent(event) {
  return new Promise((resolve) => {
    setTimeout(() => {
      events.push(event);
      resolve(true);
    }, TIMEOUT);
  });
}
