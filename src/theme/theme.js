import { createMuiTheme } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";

export default createMuiTheme({
  palette: {
    primary: {
      main: green[500],
    },
    secondary: {
      main: "#004B75",
    },
  },
});
