import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./theme/theme";
import { Layout } from "./components";
import Routes from "./routes/routes";

import { eventsActions } from "./state/events";

function App(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(eventsActions.loadAllEvents());
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <BrowserRouter>
          <Layout>
            <Routes {...props} />
          </Layout>
        </BrowserRouter>
      </div>
    </ThemeProvider>
  );
}

export default App;
