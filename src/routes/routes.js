import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { CurrentEvents, ExpiredEvents, EditEvent } from "../pages";

const Routes = () => {
  return (
    <Switch>
      <Redirect from="/" to="/current" exact />
      <Route path="/current" exact component={CurrentEvents} />
      <Route path="/recent" exact component={ExpiredEvents} />
      <Route path="/edit" exact component={EditEvent} />
      <Route path="/edit/:eventId" exact component={EditEvent} />
    </Switch>
  );
};

export default Routes;
