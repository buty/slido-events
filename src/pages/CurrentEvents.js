import React from "react";
import { useSelector } from "react-redux";
import { EventList } from "../components";
import { eventsSelectors } from "../state/events";

const CurrentEvents = () => {
  const { events, loading } = useSelector((state) =>
    eventsSelectors.selectEvents(state)
  );

  const currentEvents =
    events.length > 0 ? eventsSelectors.selectCurrentEvents(events) : events;

  return <EventList events={currentEvents} loading={loading} />;
};

export default CurrentEvents;
