import React from "react";
import { useSelector } from "react-redux";
import { EventList } from "../components";
import { eventsSelectors } from "../state/events";

const RecentEvents = () => {
  const { events, loading } = useSelector((state) =>
    eventsSelectors.selectEvents(state)
  );

  const expiredEvents =
    events.length > 0 ? eventsSelectors.selectExpiredEvents(events) : events;

  return <EventList events={expiredEvents} loading={loading} />;
};

export default RecentEvents;
