import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { editActions, editSelectors } from "../state/edit";

import { Loader, EventTextField } from "../components";

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(3),
    padding: theme.spacing(1),
    [theme.breakpoints.up("sm")]: {
      width: "80%",
      marginLeft: "auto",
      marginRight: "auto",
      marginTop: theme.spacing(6),
    },
  },
  title: {
    marginBottom: theme.spacing(2),
  },
  formContainer: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    [theme.breakpoints.up("md")]: {
      justifyContent: "flex-start",
    },
  },
  formField: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    width: "100%",
    textTransform: "capitalize",
    [theme.breakpoints.up("md")]: {
      flexBasis: "40%",
      maxWidth: 250,
      marginRight: "10%",
    },
  },
  submit: {
    marginTop: theme.spacing(4),
    color: "white",
    [theme.breakpoints.up("md")]: {
      flexBasis: "100%",
      maxWidth: 250,
    },
  },
}));

const EditEvent = () => {
  const classes = useStyles();
  const location = useLocation();
  const dispatch = useDispatch();
  const { loading, event, isCreation, validation } = useSelector((state) =>
    editSelectors.selectEvent(state)
  );

  useEffect(() => {
    // todo: refactor
    const pathName = location.pathname;
    if (pathName.includes("edit/")) {
      const eventId = pathName.replace("/edit/", "");
      dispatch(editActions.loadEventById(eventId));
    } else {
      dispatch(editActions.initNewEvent());
    }
  }, [location, dispatch]);

  return (
    <div className={classes.container}>
      <Typography
        className={classes.title}
        variant="h4"
        component="h2"
        color="primary"
      >
        {isCreation ? "New event" : "Event details"}
      </Typography>
      {loading ? (
        <Loader />
      ) : (
        <form className={classes.formContainer} autoComplete="off">
          {Object.keys(event).map((key) => (
            <EventTextField
              className={classes.formField}
              value={event[key]}
              type={key}
              disabled={!isCreation}
              key={key}
              error={validation.errors[key]}
            />
          ))}
          {isCreation ? (
            <Button
              type="submit"
              color="primary"
              className={classes.submit}
              fullWidth
              variant="contained"
              onClick={() => dispatch(editActions.saveEvent(event))}
              disabled={!validation.isValid}
            >
              Submit
            </Button>
          ) : null}
        </form>
      )}
    </div>
  );
};

export default EditEvent;
